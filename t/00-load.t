#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'App::DNS6DProxy' ) || print "Bail out!
";
}

diag( "Testing App::DNS6DProxy $App::DNS6DProxy::VERSION, Perl $], $^X" );
