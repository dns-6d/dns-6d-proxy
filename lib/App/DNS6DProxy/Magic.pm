# Copyright 2010 Malte S. Stretz <http://msquadrat.de>
#
# This work is licensed under the Apache License, Version 2.0
# (the "License"); you may not use this file except in
# compliance with the License.  You may obtain a copy of the
# License at
#   http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

package App::DNS6DProxy::Magic;

use strict;
use warnings;

use Net::CIDR;


sub resolve
{
  my($resolver, $qname, $qclass, $qtype, $qhost) = @_;
  my $prefix = $resolver->{prefix};
  my $packet;

  # Query the upstream DNS server.
  # TODO: handle null (truncated packet)
  $packet = $resolver->{instance}->send($qname, $qtype, $qclass);

  return $packet if (
  # We only hack IPv6.
                 $qtype ne 'AAAA'
  # Nothing to do if RRs were found.
              or $packet->header()->{ancount} > 0
  # Also, we've got to check for existing domains only.
              or $packet->header()->{rcode} ne 'NOERROR'
  # I don't think anybody will ever query anything else, but hey.
              or $qclass ne 'IN'
  # Make sure we don't loop by checking any DNS-6D entries...
              or substr($qname, 0, length($prefix) + 1) eq $prefix . '.'
  # .. or an aliases.
              or $resolver->{aliases}->{$qname}
  # Apply client policy.
              or $resolver->{skip}($qhost)
  );
  
  # Query the alias or the DNS-6D entry.
  # TODO: handle null (truncated packet)
  my $aname = $resolver->{aliased}->{$qname} || join('.', $prefix, $qname);
  $packet = $resolver->{instance}->send($aname, $qtype, $qclass);
  
  # Replace any references to our alias.  We access the internal buffer
  # of N::D::Packet here, which is parsed because we called the accessor
  # before.  Evil?  Well... we could also rely on answer() returning a
  # list of references (which it does) but this will break more obviously
  # in case the underlying implementation changes.  Of course we could
  # also create our own list of answers and rely on less magic.  But then
  # life would be boring.
  $packet->answer(); # parse!
  map { $_->{name} = $qname if $_->{name} eq $aname } @{$packet->{answer}};
  
  # This is all we've got.
  return $packet;
}

1;
