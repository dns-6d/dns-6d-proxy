# Copyright 2010 Malte S. Stretz <http://msquadrat.de>
#
# This work is licensed under the Apache License, Version 2.0
# (the "License"); you may not use this file except in
# compliance with the License.  You may obtain a copy of the
# License at
#   http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.


package App::DNS6DProxy;

use App::DNS6DProxy::Magic;

use strict;
use warnings;

our $VERSION = '0.04';

use File::Basename qw();
use POSIX qw(setsid);

use Net::DNS::Resolver;
use Net::DNS::Nameserver;

use Net::IP;
use Net::CIDR qw();


use constant BIND_ADDR => q{0.0.0.0};
use constant BIND_PORT => $> == 0 ? 53 : 53000;

use constant VERBOSITY => 2;

use constant SYSLOG_FACILITY => 'daemon';

use constant PREFIX => q{_ipv6};

# TODO: Get value from Build.PL
use constant CONFIG_DIR => $> == 0 ? q{/etc/dns-6d} : $ENV{HOME} . '/' . q{.dns-6d-proxy};


# TODO:  This wants to be an object.
my %resolver; %resolver = (
  prefix => PREFIX,
  
  # Global resolver, instanced on run().
  instance => undef,

  # Optional mapping of hostnames to alternative aliases (not using our 
  # PREFIX) as configured in hosts.aliases.
  aliased => {},
  # List of hosts used as aliases, used to avoid loops.
  aliases => {},

  # Lists of hosts_access(5)-like client policies to skip DNS-6D for certain
  # clients and only do the normal IPv6 lookups.
  allowed => [],
  denied  => [],
  
  # Wrap the lookup logic here.
  skip    => sub {
    my $ip = tov6(shift);
    return     Net::CIDR::cidrlookup($ip, @{$resolver{denied}})
       and not Net::CIDR::cidrlookup($ip, @{$resolver{allowed}});
  },
);

# Flags indicating SIGHUP or SIGTERM was received.
my $got_sighup = 0;
my $got_sigterm = 0;

sub run
{
  load_config();

  daemonize();
  
  my $ns = Net::DNS::Nameserver->new(
    LocalAddr       => BIND_ADDR,
    LocalPort       => BIND_PORT,
    ReplyHandler    => \&process,
    Verbose         => VERBOSITY,
     # TODO: Only available with Net::DNS 0.66, I prefer consistency over 
     # features for now, makes debugging easier.
    Truncate        => 0,
  ) or die "$!";
  
  $resolver{instance} ||= Net::DNS::Resolver->new(
    recurse => 1,
    debug   => VERBOSITY >= 2 ? 1 : 0,
    # TODO: Make this configurable instead of using (only) resolv.conf
    #nameservers => [],
  );
  
  # Cleanup nameservers to avoid loops.
  $resolver{instance}->nameservers(
    grep !/^(?:127[.]0[.]0[.]1|0[.]0[.]0[.]0|::1?)$/
      => $resolver{instance}->nameservers()) if (BIND_PORT == 53);
  
  while (!$got_sigterm) {
    $ns->loop_once(10);
    if ($got_sighup) {
      load_config();
      $got_sighup = 0;
    }
  }
}

sub process
{
  my($qname, $qclass, $qtype, $qhost, $qdata, $qsock) = @_;
  
  # The actual magic is happening somewhere else.
  my $packet = App::DNS6DProxy::Magic::resolve(
    \%resolver,
    $qname,
    $qclass,
    $qtype,
    $qhost,
  );
  
  # Transform the resolved N::D::Packet into a reply N::D::NS can parse.
  return (
    $packet->header()->{rcode},
    [ $packet->answer() ],
    [ $packet->authority() ],
    [ $packet->additional() ],
    {
      aa => $packet->header()->{aa},
      ad => $packet->header()->{ad},
      ra => $packet->header()->{ra},
    }
  );
}

sub load_config
{
  # Helper function to read a whole file to an array, removing any
  # comments (from the first # to eol) and skipping empty lines.
  sub slurp {
    my($file) = @_;
    open(my $fh, '<', CONFIG_DIR . '/' . $file) or return ();
    my @lines;
    foreach my $line (<$fh>) {
      $line =~ s/\s*(?:#.*)?$|^\s+//g;
      next unless $line;
      push(@lines, $line);
    }
    close($fh);
    return @lines;
  }
  
  # Make Net::CIDR happy and ease querying the policy by converting 
  # everything to IPv6.  Also, we have to take extra care of 0.0.0.0 
  # and ::, both meaning all hosts (netmask zero).
  sub cidrify
  {
    my @ret;
    while (@_) {
      my $ip = shift;
      $ip = $ip =~ /^(?:[0.]+|::)$/   ? "$ip/0"   :
            $ip =~ /^[[:digit:].]+$/  ? "$ip/32"  :
            $ip =~ /^[[:xdigit:]:]+$/ ? "$ip/128" :
            $ip;
      my($net, $msk) = split m{/} => $ip;
      push @ret => join '/' => tov6($net), $msk * 1 + (128 - 32);
    };
    return @ret;
  }
  
  # The client policy files have one IP or net per line, apply.
  $resolver{allowed} = [ cidrify(slurp("clients.allow")) ];
  $resolver{denied}  = [ cidrify(slurp("clients.deny"))  ];
  
  # The alias format is "alias host1 host2 hostn", split it accordingly.
  $resolver{aliases} = {};
  $resolver{aliased} = {};
  foreach my $line (slurp("hosts.aliases")) {
    my($alias, @hosts) = split(/\s+/, $line);
    next unless $alias;
    $resolver{aliases}{$alias} = 1;
    map { $resolver{aliased}{$_} = $alias } @hosts;
  }
}

sub daemonize
{
  my $appname = File::Basename::basename($0);

  chdir('/') or die "Can't chdir to root: $!";

  open(STDIN,  '<', "/dev/null") or die "Can't read from /dev/null: $!";
#  if (VERBOSITY == 0) {
    open(STDOUT, '>', "/dev/null") or die "Can't write to /dev/null: $!";
#  } else {
#    open(STDOUT, '|-', "logger -t '${appname}' -p " . SYSLOG_FACILITY . ".debug") or die "Can't open logger: $!";
#  }
 
  defined(my $pid = fork) or die "Can't fork(): $!";
  exit if $pid;
  setsid() or die "Can't setsid(): $!";
  
  open(STDERR, '>&', "STDOUT") or die "Can't duplicate etdout: $!";

  $SIG{INT} = $SIG{TERM} = sub{ $got_sigterm++ };
  $SIG{HUP} = sub { $got_sighup++ };
}

# Net::CIDR is picky when it comes to mixing IPv4 and IPv6.  So
  # cheat by converting IPv4 addresses to IPv6.
sub tov6
{
   my $ip = shift || '';
   return $ip if $ip =~ /:/;
   my @ip = (split(/[.]/, $ip), 0, 0, 0, 0);
   return unpack "AAH4AH4AH4" => pack "AAH4Ac2Ac2" => qw{: : ffff :}, @ip[0 .. 1], ':', @ip[2 .. 3];
}

1; # End of App::DNS6DProxy
